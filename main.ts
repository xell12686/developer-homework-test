import {
    GetProductsForIngredient,
    GetRecipes,
} from "./supporting-files/data-access";
import { NutrientFact, Product, Recipe, RecipeLineItem, SupplierProduct } from "./supporting-files/models";
import {
    GetCostPerBaseUnit,
    GetNutrientFactInBaseUnits,
} from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */
function calculateCheapestCostAndNutrients(recipe: Recipe) {
    const result = {
        cheapestCost: 0,
        nutrientsAtCheapestCost: {},
    };

    const lineItems = recipe.lineItems || [];

    lineItems.forEach((lineItem: RecipeLineItem) => {
        const productsForIngredient = GetProductsForIngredient(lineItem.ingredient);

        const nutrientFactsAndCheapestCosts = productsForIngredient.map(
            (product: Product) => {
                const nutrientFactsInBaseUnit =
                    product.nutrientFacts?.map((nutrientFact: NutrientFact) =>
                        GetNutrientFactInBaseUnits(nutrientFact)
                    ) || [];

                const costPerBaseUnits =
                    product.supplierProducts?.map((supplierProduct: SupplierProduct) =>
                        GetCostPerBaseUnit(supplierProduct)
                    ) || [];

                return {
                    nutrientFactsInBaseUnit,
                    cheapestCostPerBaseUnit: Math.min(...costPerBaseUnits),
                };
            }
        );

        const cheapestCost = nutrientFactsAndCheapestCosts.reduce((minCost, cost) =>
            cost.cheapestCostPerBaseUnit < minCost.cheapestCostPerBaseUnit
                ? cost
                : minCost
        );

        result.cheapestCost +=
            cheapestCost.cheapestCostPerBaseUnit *
            (lineItem.unitOfMeasure?.uomAmount ?? 0);

        cheapestCost.nutrientFactsInBaseUnit.forEach(
            (nutrientFact: NutrientFact) => {
                const resultFact: NutrientFact =
                    result.nutrientsAtCheapestCost[nutrientFact.nutrientName] ||
                    nutrientFact;
                resultFact.quantityAmount.uomAmount +=
                    nutrientFact.quantityAmount.uomAmount;
                result.nutrientsAtCheapestCost[nutrientFact.nutrientName] = resultFact;
            }
        );

        const sortedNutrients = Object.keys(result.nutrientsAtCheapestCost)
            .sort()
            .reduce((accumulator: { [key: string]: NutrientFact }, key: string) => {
                accumulator[key] = result.nutrientsAtCheapestCost[key];
                return accumulator;
            }, {});

        result.nutrientsAtCheapestCost = sortedNutrients;
    });

    return result;
}

recipeData.forEach((recipe: Recipe) => {
    recipeSummary[recipe.recipeName] = calculateCheapestCostAndNutrients(recipe);
});

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
